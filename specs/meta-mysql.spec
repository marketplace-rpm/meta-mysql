%global app                     mysql
%global user                    %{app}
%global group                   %{app}

%global d_home                  /home
%global d_storage               %{d_home}/storage_01
%global d_data                  %{d_storage}/data_02

%global d_cnf                   %{_sysconfdir}/mysql
%global d_service               %{_sysconfdir}/systemd/system/mysqld.service.d

Name:                           meta-mysql
Version:                        1.0.2
Release:                        4%{?dist}
Summary:                        META-package for install and configure MySQL
License:                        GPLv3

Source10:                       my.cnf
Source20:                       service.limits.conf

Requires:                       meta-repos mysql-community-server

%description
META-package for install and configure MySQL.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -dp -m 0755 %{buildroot}%{d_data}/%{app}

%{__install} -Dp -m 0644 %{SOURCE10} \
  %{buildroot}%{d_cnf}/my.cnf
%{__install} -Dp -m 0644 %{SOURCE20} \
  %{buildroot}%{d_service}/custom.limits.conf


%files
%attr(0700,%{user},%{group}) %dir %{d_data}/%{app}
%dir %{d_cnf}
%config %{d_cnf}/my.cnf
%config %{d_service}/custom.limits.conf


%changelog
* Sun Jul 28 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.2-4
- UPD: SPEC-file.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.2-3
- UPD: SPEC-file.
- FIX: Systemd Drop-In.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.2-2
- UPD: SPEC-file.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.2-1
- NEW: Systemd config.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-11
- UPD: MySQL config.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-10
- UPD: MySQL config.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-9
- UPD: MySQL config.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-8
- UPD: MySQL config.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-7
- UPD: MySQL config.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-6
- UPD: MySQL config.

* Fri Jul 05 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-5
- UPD: MySQL config.

* Fri Jul 05 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-4
- UPD: MySQL config.

* Fri Jul 05 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-3
- UPD: SPEC-file.

* Fri Jul 05 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-2
- UPD: MySQL config.

* Thu Jul 04 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-1
- FIX: Config files.
- UPD: SPEC-file.

* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-3
- UPD: SPEC-file.

* Mon Jul 01 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-2
- UPD: MySQL config.

* Sat Jun 22 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
